#define __NO_STD_VECTOR
#include <iostream>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/face.hpp"
#include "opencv2/objdetect.hpp"

#include <boost/compute/system.hpp>
#include <boost/compute/interop/opencv/core.hpp>
#include <boost/compute/interop/opencv/highgui.hpp>
#include <boost/compute/utility/source.hpp>

#include <boost/program_options.hpp>

namespace compute = boost::compute;
namespace po = boost::program_options;


const char source[] = BOOST_COMPUTE_STRINGIZE_SOURCE(
        __kernel void box_filter(__read_only image2d_t input,
                                 __write_only image2d_t output,
                                 uint box_height,
                                 uint box_width)
        {
            int x = get_global_id(0);
            int y = get_global_id(1);
            int h = get_image_height(input);
            int w = get_image_width(input);
            int k = box_width;
            int l = box_height;

    
                const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

                float4 sum = { 0, 0, 0, 0 };
                for(int i = 0; i < k; i++){
                    for(int j = 0; j < l; j++){
                        sum += read_imagef(input, sampler, (int2)(x+i-k, y+j-l));
                    }
                }
                sum /= (float) k * l;
                float4 value = (float4)( sum.x, sum.y, sum.z, 1.f );
                write_imagef(output, (int2)(x, y), value);
            }
    
    );


int main(int argc, char *argv[])
{
   
    po::options_description desc;
    desc.add_options()
            ("help",  "show available options")
            ("camera", po::value<int>()->default_value(-1),
                                 "if not default camera, specify a camera id")
            ("haar", po::value<std::string>(), "path to haar cascade");

    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    
    if(vm.count("help"))
    {
        std::cout << desc << std::endl;
        return 0;
    }

    
    
    cv::Mat cv_mat;
    cv::VideoCapture cap; 

    
    compute::device gpu = compute::system::default_device();
    compute::context context(gpu);
    compute::command_queue queue(context, gpu);
    compute::program filter_program =
            compute::program::create_with_source(source, context);

    try
    {
        filter_program.build();
    }
    catch(compute::opencl_error e)
    {
        std::cout<<"Build Error: "<<std::endl
                 <<filter_program.build_log();
    }


    compute::kernel filter_kernel(filter_program, "box_filter");


    
    if(vm.count("haar"))
    {
       
    }
    else
    {
        std::cout << "</path/to/haar_cascade> -- Path to the Haar Cascade for face detection." << std::endl;
        exit(1);
    }

     cap.open(vm["camera"].as<int>());
     cap >> cv_mat;
     if(!cv_mat.data){
        std::cerr << "failed to capture frame" << std::endl;
        return -1;
        }

        cv::CascadeClassifier haar_cascade;
        haar_cascade.load(vm["haar"].as<std::string>());

        char key = '\0';
        while(key != 27) //check for escape key
        {
            cap >> cv_mat;

            cv::Mat gray;
            cvtColor(cv_mat, gray, CV_BGR2GRAY);
            std::vector< cv::Rect_<int> > faces;
            haar_cascade.detectMultiScale(gray, faces);

            if(!faces.size()){
                cv::imshow("blurryface", cv_mat);
            }
            else
            {
            cv::Rect face_i = faces[0];

            cv::Mat face = cv_mat(face_i).clone();

            cv::cvtColor(face, face, CV_RGB2BGRA);


            compute::image2d dev_input_image = 
            compute::opencv_create_image2d_with_mat(
                face, compute::image2d::read_write, queue
                );

            size_t origin[2] = { 0, 0 };
            size_t region[2] = { dev_input_image.width(),
                         dev_input_image.height() };
            
            compute::image2d dev_output_image(
                context,
                face_i.width,
                face_i.height,
                dev_input_image.format(),
                compute::image2d::write_only
                );

            filter_kernel.set_arg(0, dev_input_image);
            filter_kernel.set_arg(1, dev_output_image);
            filter_kernel.set_arg(2, 50);
            filter_kernel.set_arg(3,50);
        
            compute::opencv_copy_mat_to_image(face, dev_input_image,queue);
            
            queue.enqueue_nd_range_kernel(filter_kernel, 2, origin, region, 0);

            compute::opencv_copy_image_to_mat(dev_output_image, face,queue);

            
            cv::cvtColor(face, face, CV_BGRA2RGB);

            face.copyTo(cv_mat(cv::Rect(face_i.x,face_i.y,face.size().width,face.size().height)));

            cv::imshow("blurryface", cv_mat);
        }
            key = cv::waitKey(10);
    }
    return 0;
}